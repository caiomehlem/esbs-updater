# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


**NOTE:** DO NOT EDIT! This changelog is automatically generated. See the README.md file for more information.

## [v0.5.0] - 2022-02-14
### Fixes
  - update_boot_files: Skip update if the partition is missing

### Changed
  - update_u-boot.sh: Switch boot partition while writing bootloader
  - Change the write strategy to flash all bytes together instead of small chunks
  - LICENSE: Change from AGPLv1 to GPLv2

### Added
  - validate_chain: Add option to ignore certificate validity time


## [v0.4.0] - 2021-10-15
### Fixes
- Prevent overflow of the env_write_counter
- Allow force partitioning via argument
- WROM flag not working properly on non-empty block deviice

### Added
- Support partition table files from platform-specific directories

## [v0.3.0] - 2020-06-17
### Fixes
- Always unlock eMMC boot partitions

### Added
- Get current partion cmdline
- Invalidate installation
- Safe bank swap
- Repair environment

## [v0.2.0] - 2020-04-17
### Fixes
- Negative offsets
- Post release fixes

### Added
- Support dual boot banks
- Trigger failure on worm
- Setup empty environments

## [v0.1.0] - 2020-02-18
### Added
- Validate chain of trust
- Initial update release
