#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>

DEVTMPFS_MAJOR_MINOR="0:6"
# shellcheck disable=SC2034  # Unused variables are used when sourced
MEGABYTE_SIZE=1048576
# shellcheck disable=SC2034  # Unused variables are used when sourced
METADATA_PAYLOAD_MARKER_SIZE_BYTES=4
# shellcheck disable=SC2034  # Unused variables are used when sourced
METADATA_SIGNATURE_SIZE_BYTES=4
# shellcheck disable=SC2034  # Unused variables are used when sourced
METADATA_CHAIN_SIZE_BYTES=4
# shellcheck disable=SC2034  # Unused variables are used when sourced
METADATA_SIGNATURE_SIZE_MAX=1024
PAYLOAD_MARKER=0
SOFTWARE_UPDATE_FILE="${SOFTWARE_UPDATE_FILE:-${FIXTURES?}/test_esbs-update.xz.squashfs}"
# shellcheck disable=SC2034  # Unused variables are used when sourced
SYSTEM_UPDATE_ETC_DIR="/etc/system_update/"
# shellcheck disable=SC2034  # Unused variables are used when sourced
SYSTEM_UPDATE_SCRIPT_DIR="/usr/libexec/system_update.d/"
TEST_STORAGE_BYTES_PER_SECTOR=512
# shellcheck disable=SC2034  # Unused variables are used when sourced
TEST_STORAGE_SIZE=5242880  # sectors, about 2.5 GiB


## random_int_range - generate a random number between min and max
# min:  minimum random value
# max:  maximum random value
random_int_range()
{
	_min="${1?}"
	_max="${2?}"

	if ! shuf -n 1 -i "${_min}"-"${_max}"; then
		fail "Unable to generate random number"
		return
	fi
}

## random_int - generate a random number between 0 and max
# max:  maximum random value
random_int()
{
	_max="${1?}"

	random_int_range 0 "${_max}"
}

## squashfs_inject_path - inject a file or directory into a squashfs archive
# squashfs_file:    squashfs archive to modify
# src:              source file or directory (if ending with trailing slash) to copy (recursively)
# dest:             destination location
#
# Note: Depends on the variable '${test_squashfs_root_dir}' to point to a
#       temporary directory where the archive can temporarily be extracted to.
squashfs_inject_path()
{
	_squashfs_file="${1?}"
	_src="${2?}"
	_dest="${3?}"

	rm -rf "${test_squashfs_root_dir?}"
	unsquashfs -d "${test_squashfs_root_dir}" -f "${_squashfs_file}"

	if [ "${_dest%%*/}" != "${_dest}" ]; then
	    _inject_location="${test_squashfs_root_dir}/${_dest}"
	else
	    _inject_location="${test_squashfs_root_dir}/$(dirname "${_dest}")"
	fi
	echo "Injecting '$(basename "${_src}")' into '${_inject_location}/'"
	if [ ! -d "${_inject_location}" ]; then
		mkdir -p "${_inject_location}"
	fi
	if ! cp -a "${_src}" "${test_squashfs_root_dir}/${_dest}"; then
		fail "Unable to inject file"
	fi

	unlink "${_squashfs_file}"
	mksquashfs "${test_squashfs_root_dir}" "${_squashfs_file}" -comp xz 1> "/dev/null"
}

## corrupt_bytes - corrupt with random integers in the @input_file
# input_file:           source file to be corrupted
# offset_in_file:       starting position in file to start corruption
# corruption_count:     number of bytes to corrupt (<= 4)
# corruption_top:       maximum random number to generate (<= UINT32_MAX)
# corruption_bottom:    optional minimal random number (default: 1)
#
# Note that it is up to the caller to align the top and count. E.g. if top is
# 250, a sensible count value is 1-4. Using 256 as top however, a count of 1
# will clip the input; whereas a count of 4 will zero pad to the left.
corrupt_bytes()
{
	_input_file="${1?}"
	_offset_in_file="${2?}"
	_corruption_count="${3?}"
	_corruption_top="${4?}"
	_corruption_bottom="${5:-1}"

	if ! _input_crc="$(md5sum "${_input_file}")"; then
		fail "Failed to calculate checksum for '${_input_file}'"
		return
	fi

	# Reduce the chance we can get the same random number
	for _ in $(seq 1 10); do
		printf "%08x" "$(random_int_range "${_corruption_bottom}" "${_corruption_top}")" | \
		tac -rs .. | \
		xxd -r -p | \
		dd \
		   bs="${_corruption_count}" \
		   oflag=seek_bytes \
		   conv="notrunc" \
		   count=1 \
		   of="${_input_file}" \
		   seek="${_offset_in_file}" \
		   1> "/dev/null"

		_output_crc="$(md5sum "${_input_file}")"
		if [ "${_input_crc%% *}" != "${_output_crc%% *}" ]; then
			break
		fi
	done

	if [ "${_input_crc%% *}" = "${_output_crc%% *}" ]; then
		fail "Unable to change metdata size on '${_input_file}'"
		return
	fi
}

## corrupt_file - corrupt bytes in the @input_file
# input_file:           source file to be corrupted
# output_file:          corrupted output file
# offset_in_file:       starting position in file to start corruption
# corruption_source:    source for corruption data, may be any source from /dev
# corruption_count:     number of bytes to corrupt
#
# Note, using sources other then '/dev/zero', '/dev/random' or '/dev/urandom'
# are not tested or supported.
corrupt_file()
{
	_input_file="${1?}"
	_output_file="${2?}"
	_offset_in_file="${3?}"
	_corruption_source="${4?}"
	_corruption_count="${5?}"

	if ! _input_crc="$(md5sum "${_input_file}")"; then
		fail "Failed to caluclate checksum of '${_input_file}'"
		return
	fi

	if [ "${_input_file}" != "${_output_file}" ]; then
		cp "${_input_file}" "${_output_file}"
	fi

	# Reduce the chance we can get the same random bytes
	for _ in $(seq 1 10); do
		dd \
		   bs="${_corruption_count}" \
		   oflag=seek_bytes \
		   conv="notrunc" \
		   count=1 \
		   if="/dev/${_corruption_source}" \
		   of="${_output_file}" \
		   seek="${_offset_in_file}" \
		   1> "/dev/null"

		_output_crc="$(md5sum "${_output_file}")"
		if [ "${_input_crc%% *}" != "${_output_crc%% *}" ]; then
			break
		fi
	done

	if [ "${_input_crc%% *}" = "${_output_crc%% *}" ]; then
		fail "Unable to corrupt '${_input_file}'"
		return
	fi
}

## command_placebo - override a command to always succeed.
# succeed_command:  (full)path to command that should succeed
#
# Note, that this removes the original command, and thus all invocations of
# @succeed_command succeed.
command_placebo()
{
	_succeed_command="${1?}"

	unlink "${_succeed_command}"
	cat > "${_succeed_command}" \
		<<-EOT
			#!/bin/sh
			echo "'\$(basename "\${0}")' succeeded"
			exit 0
		EOT
	chmod +x "${_succeed_command}"
}

## command_failure - override a command to always fail.
# failing_command:  (full)path to command that should fail
#
# Note, that this removes the original command, and thus all invocations of
# @failing_command fail.
command_failure()
{
	_failing_command="${1?}"

	unlink "${_failing_command}"
	cat > "${_failing_command}" \
		<<-EOT
			#!/bin/sh
			echo "'\$(basename "\${0}")' failed"
			exit 1
		EOT
	chmod +x "${_failing_command}"
}

## mount_chroot_test_env - mount the update archive and its dependent devices
# swupdate_file:        squashfs based software update file
# update_mountpoint:    mountpoint to use as the update base
#
# Note, that @update_mountpoint is to be created (mktemp) by the caller.
mount_chroot_test_env()
{
	_swupdate_file="${1?}"
	_update_mountpoint="${2?}"

	_swupdate_mountpoint="/tmp/$(basename "${_swupdate_file}")"
	_overlay_mountpoint="${_update_mountpoint}"

	# Without a real '/dev' loop device partitions are not accessible
	if [ "$(mountpoint -d -q "/dev")" != "${DEVTMPFS_MAJOR_MINOR}" ]; then
		mount -t devtmpfs devfs "/dev"
		dev_mounted="true"
	fi

	# Note: normally upper is on persistent storage, fake it on tmpfs instead
	mount -o noatime,mode=0755 -t tmpfs tmpfs "${_update_mountpoint}"
	install -D -d -m 755 \
	        "${_update_mountpoint}/overlay/rom" \
	        "${_update_mountpoint}/overlay/upper" \
	        "${_update_mountpoint}/overlay/work"
	mount -t squashfs "${_swupdate_file}" "${_update_mountpoint}/overlay/rom"
	mount \
	      -t overlay \
	      -o "lowerdir=${_update_mountpoint}/overlay/rom,upperdir=${_update_mountpoint}/overlay/upper,workdir=${_update_mountpoint}/overlay/work" \
	      "overlayfs:${_overlay_mountpoint}" \
	      "${_overlay_mountpoint}"

	mount -o noatime,mode=0755 -t tmpfs tmpfs "${_update_mountpoint}/dev"
	install -D -d -m 755 \
	        "${_overlay_mountpoint}/dev/lower" \
	        "${_overlay_mountpoint}/dev/upper" \
	        "${_overlay_mountpoint}/dev/work"
	mount -t devtmpfs devfs "${_overlay_mountpoint}/dev/lower"
	mount \
	      -t overlay \
	      -o "lowerdir=${_update_mountpoint}/dev/lower,upperdir=${_update_mountpoint}/dev/upper,workdir=${_update_mountpoint}/dev/work" \
	      "overlayfs:dev" \
	      "${_overlay_mountpoint}/dev"
	mount -t devpts devpts "${_overlay_mountpoint}/dev/pts"

	mount -t proc proc "${_overlay_mountpoint}/proc"

	mount -o noatime,mode=0755 -t tmpfs tmpfs "${_update_mountpoint}/sys"
	install -D -d -m 755 \
	        "${_overlay_mountpoint}/sys/lower" \
	        "${_overlay_mountpoint}/sys/upper" \
	        "${_overlay_mountpoint}/sys/work"
	mount -t sysfs sysfs "${_overlay_mountpoint}/sys/lower"
	mount \
	      -t overlay \
	      -o "lowerdir=${_update_mountpoint}/sys/lower,upperdir=${_update_mountpoint}/sys/upper,workdir=${_update_mountpoint}/sys/work" \
	      "overlayfs:sys" \
	      "${_overlay_mountpoint}/sys"

	mount -t tmpfs tmpfs "${_overlay_mountpoint}/run"

	mount -t tmpfs tmpfs "${_overlay_mountpoint}/tmp"

	touch "${_overlay_mountpoint}/${_swupdate_mountpoint}"
	mount -o "bind,ro" "${_swupdate_file}" "${_overlay_mountpoint}/${_swupdate_mountpoint}"
}

## umount_chroot_test_env - unmount the update archive and its dependent devices
# swupdate_file:        squashfs based update file to unmount
# update_mountpoint:    mountpoint where the @swupdate_file is mounted
#
# Note, that @update_mountpoint is to be unlinked by the caller.
umount_chroot_test_env()
{
	_swupdate_file="${1?}"
	_update_mountpoint="${2?}"

	_overlay_mountpoint="${_update_mountpoint}"
	_swupdate_file_mountpoint="${_overlay_mountpoint}/tmp/$(basename "${_swupdate_file}")"

	for _ in $(seq 1 300); do
		if mountpoint -q "${_swupdate_file_mountpoint}"; then
			umount "${_swupdate_file_mountpoint}"
		fi

		if [ -d "${_swupdate_file_mountpoint}" ]; then
			unlink "${_swupdate_file_mountpoint}"
		fi

		for _mountpoint in \
		                   "/tmp" \
		                   "/run" \
		                   "/sys" "/sys/lower" "/sys" \
		                   "/proc" \
		                   "/dev/pts" "/dev" "/dev/lower" "/dev" \
		                   "/overlay/rom" \
		                   ""; do
			if mountpoint -q "${_overlay_mountpoint}${_mountpoint}"; then
				umount "${_overlay_mountpoint}${_mountpoint}"
			fi
		done

		if mountpoint -q "${_update_mountpoint}"; then
			umount "${_update_mountpoint}"
		fi

		if ! mountpoint -q "${_update_mountpoint}"; then
			_chroot_env_unmounted="true"
			break
		fi

		echo "Retrying cleanup of '${_update_mountpoint}', this can happen on slow media"
		sleep 1
	done

	if [ "${_chroot_env_unmounted:-}" != "true" ]; then
		echo "Giving up cleaning up '${_update_mountpoint}', manual cleanup may be needed."
	fi

	if [ "$(mountpoint -d -q "/dev")" = "${DEVTMPFS_MAJOR_MINOR}" ] && \
	   [ "${dev_mounted:-}" = "true" ]; then
		umount "/dev"
		unset dev_mounted
	fi
}

## create_dummy_storage_device - create a dummy storage device
# dummy_storage_device_img: temporary file to allocate and mount loop device on
#
# Note: Sets the global variable '${test_dummy_storage_device}' containing the
# created loop device
# Cleanup is to be done using destroy_dummy_storage_device.
create_dummy_storage_device()
{
	_dummy_storage_device_img="${1?}"
	_dummy_storage_size="${2:-$((TEST_STORAGE_BYTES_PER_SECTOR * TEST_STORAGE_SIZE))}"

	if [ ! -f "${_dummy_storage_device_img}" ]; then
		fail "Missing storage device image"
		return
	fi
	echo "Creating test storage device"

	if ! fallocate -l "${_dummy_storage_size}" "${_dummy_storage_device_img}"; then
		fail "Unable to allocate target storage device '${_dummy_storage_device_img}'"
		return
	fi

	test_dummy_storage_device="$(losetup --find --partscan --show "${_dummy_storage_device_img}")"
	echo "Loop device: ${test_dummy_storage_device}"
	if [ ! -b "${test_dummy_storage_device}" ] && \
	   [ "${test_dummy_storage_device#/dev/loop*}" = "${test_dummy_storage_device}" ]; then
		fail "The device '${test_dummy_storage_device}' does not appear to be a valid loop device"
		return
	fi
}

## destroy_dummy_storage_device - removes a previously created loop device
# dummy_storage_device_img: loop device image to be destroyed
#
# Note: Also removes the previously allocated '${dummy_storage_device_img}'.
destroy_dummy_storage_device()
{
	_dummy_storage_device_img="${1?}"
	_target_devices="$(losetup --associated "${_dummy_storage_device_img}" --noheadings --output 'NAME' --raw)"

	for _target_device in ${_target_devices}; do
		if ! losetup --detach "${_target_device}"; then
			fail "Unable to detach loop device '${_target_device}'"
		fi
	done

	if [ -f "${_dummy_storage_device_img}" ]; then
		unlink "${_dummy_storage_device_img}"
	fi
}

## append_payload_marker - append a 4 bytes 0 marker to a file
# input_file:   file to which to append the marker
append_payload_marker()
{
	_input_file="${1?}"

	printf "%08x" "${PAYLOAD_MARKER}" | xxd -r -p >> "${_input_file}"
}

## append_metadata - append the given metadata and the size of the metadata
# input_file:   file to which to append the metadata
# ...:          files to append to @input_file
#
# For each file, a 4 byte size will be recorded and appended to @input_file.swu
# Note; that the sizes exceeding UINT32_MAX are not supported
append_metadata()
{
	_input_file="${1?}"
	shift

	if [ "${#}" -lt 1 ]; then
		fail "Need to append at least one file."
		return
	fi

	{
		for _meta_file in "${@}"; do
			cat "${_meta_file}"
			printf "%08x" "$(stat -c "%s" "${_meta_file}")" | \
			                 tac -rs .. | \
			                 xxd -r -p
		done
	} >> "${_input_file}"
}

## sign_files - calculate the digest using the supplied private key
# private_key:  private key used to sign the file(s) with.
# ...:          files to sign
#
# Produces digital signatures using the original file name appended with '.sig'
sign_files()
{
	_private_key="${1?}"
	shift

	if [ "${#}" -lt 1 ]; then
		fail "Need at least one file to sign."
		return
	fi

	for _sign_file in "${@}"; do
		if ! openssl dgst \
		   -sha512 \
		   -sign "${_private_key}" \
		   -out "${_sign_file}.sig" \
		   "${_sign_file}"; then
			fail "Unable to sign file '${_sign_file}'"
			return
		fi
	done
}

## create_simple_keypair - create a basic public and private SSL key
# private_key:  filename to place the private key in
# public_key:   filename to place the public key in
#
# Note: the filename needs to already exist, as well as cleanup afterwards.
# Best use mktemp for this purpose.
create_simple_keypair()
{
	_private_key="${1?}"
	_public_key="${2?}"

	openssl genpkey \
	        -algorithm "RSA" \
	        -out "${_private_key}"

	openssl pkey \
	        -in "${_private_key}" \
	        -out "${_public_key}" \
	        -pubout
}
