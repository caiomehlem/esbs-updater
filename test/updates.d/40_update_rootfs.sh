#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%${1##*/}}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../../scripts/updates.d/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/../fixtures/}"

# shellcheck source=test/common.inc.sh
. "${src_dir}/../common.inc.sh"

set +eu


validate_rom_bank()
{
	_swupdate_file="${1?Missing argument to function}"
	_target_device="${2?Missing argument to function}"
	_rom_bank="rom_${3?Missing argument to function}"

	_update_file_size="$(stat -c "%s" "${_swupdate_file}")"
	_firmware_partition="$(sfdisk --dump --quiet "${_target_device}" | \
	                       sed -n 's|^[[:space:]]*\(/dev/[[:alnum:]]\+\)[[:space:]]*:.*name=[[:space:]]*\"\?'"${_rom_bank}"'\"\?.*$|\1|p')"

	if ! head -c "${_update_file_size}" "${_firmware_partition}" | \
	   cmp -l "${_swupdate_file}"; then
		fail "Written data does not match update file"
	fi

	echo "Matching update data"
}

oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	test_squashfs_root_dir="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_squashfs_root_dir.XXXXXX")"
	squashfs_inject_path "${swupdate_src}" \
	                     "${COMMAND_UNDER_TEST}" \
	                     "${SYSTEM_UPDATE_SCRIPT_DIR}"
	COMMAND_UNDER_TEST="${SYSTEM_UPDATE_SCRIPT_DIR}/$(basename "${COMMAND_UNDER_TEST}")"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -d "${test_squashfs_root_dir:-}" ]; then
		rm -rf "${test_squashfs_root_dir?}"
	fi

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi
}

setUp()
{
	echo

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"

	test_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_target_device_img.XXXXXX")"
	create_dummy_storage_device "${test_target_device_img}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/emmc_partition_table_template.sfdisk"

	# Do not store the update mount in the SHUNIT tmpdir, as unexpected failures
	# will trigger an rm -rf of the SHUNIT tmpdir, including the mounted /dev,
	# causing the host system to fail terribly. As neither tearDown nor
	# oneTimeTeardown is called; this will leave cruft in case of failures.
	# Until shunit2 implements a tearDown hook this cannot be avoided.
	test_update_mountpoint="$(mktemp -d -p "${TMPDIR:-/tmp}" "test_update_mountpoint.XXXXXX")"

	mount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"
}

tearDown()
{
	umount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"

	destroy_dummy_storage_device "${test_target_device_img}"

	if [ -f "${test_swupdate_file:-}" ]; then
		unlink "${test_swupdate_file}"
	fi

	if mountpoint -q "${test_update_mountpoint:-}"; then
		umount "${test_update_mountpoint}"
	fi

	if [ -d "${test_update_mountpoint}" ]; then
		rmdir "${test_update_mountpoint}"
	fi

	echo "--------------------------------------------------------------------------------"
}

testWritePrimaryRootFS()
{
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Correct rootfs write script should be accepted." "[ ${?} -eq 0 ]"
	validate_rom_bank "${test_swupdate_file}" "${test_dummy_storage_device}" "a"
}

testWritePrimaryRootFSBankB()
{
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "b" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Correct rootfs write script should be accepted." "[ ${?} -eq 0 ]"
	validate_rom_bank "${test_swupdate_file}" "${test_dummy_storage_device}" "b"
}

testMissingTargetBank()
{
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Missing target bank should fail" "[ ${?} -eq 0 ]"
}

testMissingROMPartitionName()
{
	flock "${test_dummy_storage_device}" \
	    sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/emmc_romless_partition_table_template.sfdisk"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Missing ROM partition should not be accepted." "[ ${?} -eq 0 ]"
}

testDataWriteFailure()
{
	command_failure "${test_update_mountpoint}/bin/dd"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Data write should fail." "[ ${?} -eq 0 ]"
}

testDataCompareFailure()
{
	command_failure "${test_update_mountpoint}/usr/bin/cmp"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Data compare should fail." "[ ${?} -eq 0 ]"
}

testMountFailure()
{
	command_failure "${test_update_mountpoint}/bin/mount"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Mounting should fail." "[ ${?} -eq 0 ]"
}
