#!/bin/sh

set -eu

while getopts "d:" options; do
	case "${options}" in
	d)
		target_device="${OPTARG}"
		;;
	*)
		exit 1
		;;
	esac
done
shift "$((OPTIND - 1))"

swupdate_file="${1?Missing software update file.}"

if [ ! -f "${swupdate_file}" ]; then
	e_err "Update '${swupdate_file}' is not a file"
	exit 1
fi

if [ -n "${target_device:-}" ] && [ ! -b "${target_device:-}" ]; then
	e_err "Target storage device '${target_device:-}' is not a block device"
	exit 1
fi

unsquashfs -l "${swupdate_file}" 1> "/dev/null"

echo "Performing system update ..."

echo "Done"

exit 0
