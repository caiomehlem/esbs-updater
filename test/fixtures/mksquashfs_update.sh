#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

depends=""
# shellcheck source=/dev/null
. "$(dirname "${0}")/../../APKBUILD.in" || true

set -eu

dest="${1:-test_esbs-update.xz.squashfs}"
repositories="${2:-http://dl-cdn.alpinelinux.org/alpine/latest-stable/}"

build_dir="$(mktemp -d -p "${TMPDIR:-/tmp}" "build_dir.XXXXXX")"
target_arch="${ARCH:-armv7}"

cleanup()
{
	if [ -d "${build_dir:-}" ] && [ -z "${build_dir##*build_dir.*}" ]; then
		rm -rf "${build_dir:?}"
	fi
}

trap cleanup EXIT

if [ "$(id -u)" -ne 0 ]; then
	echo "This script requires administrate privileges."
	echo "Re-run with for example 'sudo ${0}'"
	exit 1
fi

chmod 755 "${build_dir}"

# shellcheck disable=SC2046  # allow word splitting for ${depends}
apk add \
    --allow-untrusted \
    --arch "${target_arch}" \
    --initdb \
    --root "${build_dir}" \
    --update-cache \
    --repository "${repositories}/main" \
    --repository "${repositories}/community" \
    alpine-keys $(echo "${depends}" | tr -s ' ,\f\n\t\v' ' ')

apk fetch \
    --allow-untrusted \
    --arch "${target_arch}" \
    --root "${build_dir}" \
    --repository "${repositories}/main" \
    --stdout \
    alpine-base | tar -xvz -C "${build_dir}" etc

if [ -e "${dest}" ]; then
	echo "********** NOTICE **********"
	echo "The file '${dest}' already exists,"
	echo "renaming to '${dest}.bak'."
	echo "****************************"
	mv "${dest}" "${dest}.bak"
fi

install -D -d -m 555 \
             "${build_dir}/dev" \
             "${build_dir}/proc" \
             "${build_dir}/run" \
             "${build_dir}/sys"

install -d -m 1777 "${build_dir}/var/tmp"
install -d -m 1777 "${build_dir}/run/lock"

ln -sf "../proc/self/mounts" "${build_dir}/etc/mtab"

mksquashfs "${build_dir}" "${dest}" -comp xz

cleanup

exit 0
