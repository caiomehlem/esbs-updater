#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%"${1##*'/'}"}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../scripts/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/fixtures/}"

set +eu

# shellcheck source=test/common.inc.sh
. "${src_dir}/common.inc.sh"

oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	test_private_key="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.priv.pem.XXXXXX")"
	test_public_key="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.pub.pem.XXXXXX")"
	test_squashfs_root_dir="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_squashfs_root_dir.XXXXXX")"


	create_simple_keypair "${test_private_key}" "${test_public_key}"
	append_payload_marker "${swupdate_src}"
	sign_files "${test_private_key}" "${swupdate_src}"
	append_metadata "${swupdate_src}" "${swupdate_src}.sig"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -d "${test_squashfs_root_dir:-}" ]; then
		rm -rf "${test_squashfs_root_dir?}"
	fi

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi

	if [ -f "${swupdate_src}.sig" ]; then
		unlink "${swupdate_src}.sig"
	fi

	if [ -f "${swupdate_src}.swu" ]; then
		unlink "${swupdate_src}.swu"
	fi

	if [ -f "${test_public_key}" ]; then
		unlink "${test_public_key}"
	fi

	if [ -f "${test_private_key}" ]; then
		unlink "${test_private_key}"
	fi
}

setUp()
{
	echo

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"

	test_squashfs_root_dir="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_squashfs_root_dir.XXXXXX")"

	test_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_device_img.XXXXXX")"
	create_dummy_storage_device "${test_target_device_img}"
}

tearDown()
{
	destroy_dummy_storage_device "${test_target_device_img}"

	if [ -f "${test_swupdate_file:-}" ]; then
		unlink "${test_swupdate_file}"
	fi

	echo "--------------------------------------------------------------------------------"
}

testValidSWUpdateFile()
{
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${FIXTURES}/scripts/system_update.sh" \
	                     "/usr/sbin/"

	"${COMMAND_UNDER_TEST}" "${test_swupdate_file}"
	assertTrue "Correct update should be accepted." "[ ${?} -eq 0 ]"
}

testValidSWUpdateFileWithTargetDeviceOption()
{
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${FIXTURES}/scripts/system_update.sh" \
	                     "/usr/sbin/"

	"${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" "${test_swupdate_file}"
	assertTrue "Correct update should be accepted." "[ ${?} -eq 0 ]"
}

testExcessiveParameters()
{
	"${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" "${test_swupdate_file}" "dummy1"
	assertFalse "Excessive parameter should not be accepted." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" "${test_swupdate_file}" "dummy1" "dummy2"
	assertFalse "Excessive parameters should not be accepted." "[ ${?} -eq 0 ]"
}

testLockFailure()
{
	touch "/run/$(basename "${COMMAND_UNDER_TEST}").lock"

	"${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" "${test_swupdate_file}"
	assertFalse "Update should not continue with existing lock." "[ ${?} -eq 0 ]"

	unlink "/run/$(basename "${COMMAND_UNDER_TEST}").lock"
}

testCorruptSquashFSHeader()
{
	_squashfs_superblock_size=24

	dd \
	   bs="${_squashfs_superblock_size}" \
	   conv="notrunc" \
	   count=1 \
	   if="/dev/urandom" \
	   of="${test_swupdate_file}" \
	   seek=0 \
	   1> "/dev/null"

	"${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" "${test_swupdate_file}"
	assertFalse "Corrupt update should not be accepted." "[ ${?} -eq 0 ]"
}

testCustomUpdateScript()
{
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${FIXTURES}/scripts/custom_update.sh" \
			     "/usr/sbin/"

	"${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" -s "/usr/sbin/custom_update.sh" "${test_swupdate_file}"
	assertTrue "Correct custom update should be accepted." "[ ${?} -eq 0 ]"
}

testCustomFailingUpdate()
{
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${FIXTURES}/scripts/update_failure.sh" \
	                     "/usr/sbin/"

	"${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" -s "/usr/sbin/update_failure.sh" "${test_swupdate_file}"
	assertFalse "Failing update should fail." "[ ${?} -eq 0 ]"
}

testInvalidUpdateScript()
{
	"${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" -s "/dev/console" "${test_swupdate_file}"
	assertFalse "Failing update should fail." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" -s "/etc/issue" "${test_swupdate_file}"
	assertFalse "Failing update should fail." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" -s "/tmp" "${test_swupdate_file}"
	assertFalse "Failing update should fail." "[ ${?} -eq 0 ]"
}
