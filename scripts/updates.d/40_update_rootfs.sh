#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DD_BLOCKSIZE=1048576  # 1 MiB seems to reasonable
REQUIRED_COMMANDS="
	[
	break
	command
	continue
	dd
	echo
	exit
	getopts
	grep
	mktemp
	mount
	mountpoint
	printf
	readlink
	rmdir
	sed
	seq
	set
	sfdisk
	shift
	sleep
	test
	umount
	unlink
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS] UPDATE_FILE"
	echo "Write the squashfs rootfs to the target device"
	echo "    -b Mandatory target bank"
	echo "    -d Mandatory target device"
	echo "    -h Print this help text and exit"
	echo
	echo "UPDATE_FILE, squashfs based software update file to update with."
	echo "Warning: This script is destructive and can destroy your data."
}

cleanup()
{
	for _ in $(seq 1 300); do
		if mountpoint -q "${verification_mountpoint}"; then
			umount "${verification_mountpoint}"
		fi

		if [ -d "${verification_mountpoint}" ]; then
			rmdir "${verification_mountpoint}"
		fi

		if [ ! -d "${verification_mountpoint}" ]; then
			break
		fi

		e_warn "Failed to cleanup '${verification_mountpoint}', this can happen on slow media, retrying"
		sleep 1
	done

	if [ -d "${verification_mountpoint}" ]; then
		e_err "Unable to cleanup '${verification_mountpoint}', giving up."
	fi

	trap EXIT
}

init()
{
	trap cleanup EXIT

	verification_mountpoint="$(mktemp -d -p "${TMPDIR:-/tmp}" "verification_mountpoint.XXXXXX")"
}

update_rootfs()
{
	_target_device="${1?Missing argument to function}"
	_rom_bank="rom_${2?Missing argument to function}"

	_update_file_size="$(stat -c "%s" "${update_file}")"
	_firmware_partition="$(sfdisk --dump --quiet "${_target_device}" | \
	                       sed -n 's|^[[:space:]]*\(/dev/[[:alnum:]]\+\)[[:space:]]*:.*name=[[:space:]]*\"\?'"${_rom_bank}"'\"\?.*$|\1|p')"

	if [ -z "${_firmware_partition}" ]; then
		e_err "Disk '${_target_device}' does not contain a partition named '${_rom_bank}', cannot continue"
		exit 1
	fi

	for _ in $(seq 1 10); do
		if ! dd \
		        bs="${DD_BLOCKSIZE?}" \
		        conv="fsync,notrunc" \
		        if="${update_file}" \
		        of="${_firmware_partition}" \
		        1> "/dev/null"; then
			e_warn "Failure writing root filesystem '${update_file}' to '${_firmware_partition}', retrying"
			continue
		fi

		if head -c "${_update_file_size}" "${_firmware_partition}" | cmp -s "${update_file}"; then
			_update_compare_success="true"
			break
		fi

		e_warn "Verification of root filesystem failed, retrying"
		sleep 1
	done

	if [ "${_update_compare_success:-}" != "true" ]; then
		e_err "Failed to correctly write update '${update_file}' to '${_firmware_partition}', cannot continue"
		exit 1
	fi

	if ! mount "${_firmware_partition}" "${verification_mountpoint}"; then
		e_err "Unable to mount '${_firmware_partition}' on '${verification_mountpoint}', cannot continue"
		exit 1
	fi

	for _release_info in "${verification_mountpoint}/etc/"*"-release"; do
		if [ ! -f "${_release_info}" ]; then
			continue
		fi

		echo "******************************"
		echo "Release information of '$(basename "${_release_info}")'"
		cat "${_release_info}"
		echo "******************************"
		echo
	done

	if ! umount "${verification_mountpoint}"; then
		e_warn "Failed to unmount rom '${_firmware_partition}' from '${verification_mountpoint}'"
	fi
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":b:d:fh" options; do
		case "${options}" in
		b)
			target_bank="${OPTARG}"
			;;
		d)
			target_device="${OPTARG}"
			;;
		f)
			echo "Failure flag not implemented for '${0}', ignoring"
			;;
		h)
			usage
			exit 0
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ "${#}" -lt 1 ]; then
		e_err "Missing parameter"
		usage
		exit 1
	fi

	if [ "${#}" -gt 1 ]; then
		e_err "Too many parameters"
		usage
		exit 1
	fi

	update_file="${1}"

	if [ ! -f "${update_file:-}" ]; then
		e_err "Unable to read '${update_file}', cannot continue"
		usage
		exit 1
	fi

	if [ -z "${target_device:-}" ]; then
		e_err "Missing target storage device"
		usage
		exit 1
	fi

	if [ -z "${target_bank:-}" ] ; then
		e_err "Missing target bank"
		usage
		exit 1
	fi

	check_requirements
	init
	update_rootfs "${target_device}" "${target_bank}"
	cleanup
}

main "${@}"

exit 0
