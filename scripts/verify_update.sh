#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_SSL_PUBLIC_KEY="/etc/system_update/firmware_update_key.pem"
METADATA_SIGNATURE_SIZE_BYTES=4
METADATA_SIGNATURE_SIZE_MAX=1024
REQUIRED_COMMANDS="
	[
	command
	echo
	exit
	getopts
	head
	mktemp
	od
	openssl
	printf
	set
	shift
	tail
	test
	tr
	trap
	unlink
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS] UPDATE_FILE"
	echo "Validates the supplied firmware update file."
	echo "    -h Print this help text and exit"
	echo "    -p PEM formatted Public SSL key file (default: '${DEF_SSL_PUBLIC_KEY}') [SSL_PUBLIC_KEY]"
	echo
	echo "UPDATE_FILE, software update file to update with."
	echo "All options listed between [brackets] can also be passed in environment variables."
}

cleanup()
{
	if [ -f "${firmware_signature:-}" ]; then
		unlink "${firmware_signature}"
	fi

	trap EXIT
}

init()
{
	trap cleanup EXIT

	if [ ! -f "${ssl_public_key:-}" ]; then
		e_err "No public SSL key, cannot continue"
		exit 1
	fi

	if [ ! -f "${update_file:-}" ]; then
		e_err "Unable to read '${update_file}', cannot continue"
		exit 1
	fi

	firmware_signature="$(mktemp -p "${TMPDIR:-/tmp}" "firmware_signature.XXXXXX")"

	echo "Verifying firmware update ..."
}

extract_metadata()
{
	signature_size="$((0x$(tail -c "${METADATA_SIGNATURE_SIZE_BYTES}" "${update_file}" | \
	                       od -An -t x4 | \
	                       tr -d " \t")))"
	if [ "${signature_size}" -gt "${METADATA_SIGNATURE_SIZE_MAX}" ] || \
	   [ "${signature_size}" -le 0 ]; then
		e_err "Unexpected signature size '${signature_size}'"
		exit 1
	fi
	metadata_size="$((signature_size + METADATA_SIGNATURE_SIZE_BYTES))"

	tail -c "${metadata_size}" "${update_file}" | \
	head -c "${signature_size}" > "${firmware_signature}"
}

verify_update()
{
	if ! head -c "-${metadata_size}" "${update_file}" | \
	   openssl dgst \
	           -sha512 \
	           --keyform "DER" \
	           -verify "${ssl_public_key}" \
	           -signature "${firmware_signature}" \
	           "-" && \
	   ! head -c "-${metadata_size}" "${update_file}" | \
	   openssl dgst \
	           -sha512 \
	           --keyform "PEM" \
	           -verify "${ssl_public_key}" \
	           -signature "${firmware_signature}" \
	           "-"; then
		e_err "Invalid or corrupt software update file"
		exit 1
	fi

	echo "SUCCESS: Update file valid"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":hp:" options; do
		case "${options}" in
		h)
			usage
			exit 0
			;;
		p)
			ssl_public_key="${OPTARG}"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument"
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ "${#}" -lt 1 ]; then
		e_err "Missing parameter"
		usage
		exit 1
	fi

	if [ "${#}" -gt 1 ]; then
		e_err "Too many parameters"
		usage
		exit 1
	fi

	update_file="${1}"
	ssl_public_key="${ssl_public_key:-${SSL_PUBLIC_KEY:-${DEF_SSL_PUBLIC_KEY}}}"

	check_requirements
	init
	extract_metadata
	verify_update
	cleanup
}

main "${@}"

exit 0
