#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>

set -eu

DEF_UPDATE_SCRIPT="/usr/sbin/system_update.sh"
REQUIRED_COMMANDS="
	[
	break
	command
	echo
	exit
	getopts
	mktemp
	mount
	mountpoint
	printf
	readlink
	rmdir
	seq
	set
	shift
	test
	trap
	umount
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS] SWUPDATE_FILE"
	echo "Validates the supplied firmware update file."
	echo "    -d Target storage device"
	echo "    -f Force failures where failures would be accepted (ex. WORM)"
	echo "    -h Print this help text and exit"
	echo "    -s Optional script inside the update to run (default: '${DEF_UPDATE_SCRIPT}')"
	echo
	echo "SWUPDATE_FILE, software update file to update with."
}

cleanup()
{
	for _ in $(seq 1 300); do
		if mountpoint -q "${mountpoint_update_src:-}"; then
			echo "Cleaning up mountpoint '${mountpoint_update_src}'"
			umount "${mountpoint_update_src}"
		fi

		if [ -d "${mountpoint_update_src:-}" ]; then
			echo "Removing mountpoint '${mountpoint_update_src}'"
			rmdir "${mountpoint_update_src}"
		fi

		if [ ! -d "${mountpoint_update_src:-}" ]; then
			break
		fi

		e_warn "Failed to cleanup '${mountpoint_update_src}', this can happen on slow media, retrying ..."
		sleep 1
	done

	if [ -d "${mountpoint_update_src:-}" ]; then
		e_warn "Unable to cleanup '${mountpoint_update_src}', giving up."
	fi

	if [ -f "${swupdate_lockfile}" ]; then
		unlink "${swupdate_lockfile}"
	fi

	trap EXIT
}

init()
{
	# Avoid cleaning up existing locks through sprung cleanup trap
	swupdate_lockfile="/run/$(basename "$(readlink -f "${0}")").lock"
	if [ -e "${swupdate_lockfile}" ] || ! echo "${$}" > "${swupdate_lockfile}"; then
		e_err "Unable to get exclusive lock using '${swupdate_lockfile}'"
		exit 1
	fi

	trap cleanup EXIT

	echo "Initializing software update using '${swupdate_file}'"

	if [ -z "${update_script:-}" ]; then
		update_script="${DEF_UPDATE_SCRIPT}"
	fi

	mountpoint_update_src="$(mktemp -d -p "${TMPDIR:-/tmp}" "mountpoint_update_src.XXXXXX")"
	if ! mount -o 'exec,suid' "${swupdate_file}" "${mountpoint_update_src}"; then
		e_err "Failed to mount update file"
		exit 1
	fi

	echo "Successfully mounted update file on '${mountpoint_update_src}'"
}

run_update()
{
	_swupdate_file="${1?Missing argument to function}"
	_update_script="${2?Missing argument to function}"
	_target_device="${3:-}"
	echo "Running update script in archive"

	if [ ! -f "${mountpoint_update_src}/${_update_script}" ] || \
	   [ ! -x "${mountpoint_update_src}/${_update_script}" ]; then
		e_err "The update script '${_update_script}' in '${mountpoint_update_src}' is not an executable file, cannot continue"
		exit 1
	fi

	if ! "${mountpoint_update_src}/${_update_script}" \
	                                                   ${_target_device:+-d "${_target_device}"} \
	                                                   ${force_failure:+-f} \
	                                                   "${_swupdate_file}"; then
		e_err "The update script '${_update_script}' in '${mountpoint_update_src}' failed with an error"
		exit 1
	fi

	echo "Update completed"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":d:fhs:" options; do
		case "${options}" in
		d)
			target_device="${OPTARG}"
			;;
		f)
			force_failure="true"
			;;
		h)
			usage
			exit 0
			;;
		s)
			update_script="$(readlink -f "${OPTARG}")"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ "${#}" -lt 1 ]; then
		echo "Error: Missing parameter."
		usage
		exit 1
	fi

	if [ "${#}" -gt 1 ]; then
		echo "Error: Too many parameters."
		usage
		exit 1
	fi

	swupdate_file="${1:-}"

	update_script="${update_script:-${DEF_UPDATE_SCRIPT}}"

	check_requirements

	if [ ! -f "${swupdate_file}" ]; then
		e_err "Update '${swupdate_file}' is not a file"
		usage
		exit 1
	fi

	init
	run_update "${swupdate_file}" "${update_script}" "${target_device:-}"
	cleanup
}

main "${@}"

exit 0
