#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_UBOOT_PATH="/usr/share/u-boot/"
DEF_UBOOT_ENV_VER="env_version=0"
REQUIRED_COMMANDS="
	[
	blkid
	break
	command
	dd
	echo
	exit
	flash_erase
	fw_printenv
	fw_setenv
	getopts
	grep
	mkenvimage
	mktemp
	mmc
	mount
	mountpoint
	printf
	rmdir
	seq
	set
	shift
	sleep
	tac
	test
	umount
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo "Install files from '${DEF_UBOOT_PATH}/<compatibles>' to the device's boot location(s)"
	echo "based on the boards compatible in sysfs"
	echo "    -f Forcefully fail on WORM write failures [WORM_FAILS]"
	echo "    -h Print this help text and exit"
	echo "    -u Alternative U-Boot path to search for files [UBOOT_PATH]"
	echo "    -y Dry run (do not actually write, print what would be written"
	echo
	echo "Warning: This script is destructive and can destroy your data."
}

is_integer()
{
	__val="${1:?Missing argument ot function}"

	test "$((__val))" -eq "$((__val))" 2> "/dev/null"
}

init()
{
	trap cleanup EXIT

	boot_partition_mountpoint="$(mktemp -d -p "${TMPDIR:-/tmp}" "boot_partition_mountpoint.XXXXXX")"
	env_temp_location="$(mktemp -p "${TMPDIR:-/tmp}" "env_temp_location.XXXXXX")"
}

cleanup()
{
	if [ -f "${env_temp_location:-}" ]; then
		unlink "${env_temp_location}"
	fi

	if mountpoint -q "${boot_partition_mountpoint:-}"; then
		umount "${boot_partition_mountpoint}"
	fi

	if [ -n "${unlocked_boot_device:-}" ]; then
		for _boot_device in ${unlocked_boot_device}; do
			lock_boot_device "${_boot_device}"
		done
	fi

	if [ -d "${boot_partition_mountpoint:-}" ]; then
		rmdir "${boot_partition_mountpoint}"
	fi

	trap EXIT
}

mount_boot()
{
	_target_device="${1?Missing argument to function}"
	_mountpoint="${2:-${boot_partition_mountpoint}}"

	if [ "${_target_device%p?}" != "${_target_device}" ]; then
		_boot_partition="${_target_device}"
	else
		_boot_bank="boot_a"
		_boot_partition="$(blkid "${_target_device}"* | \
		                   sed -n 's|^[[:space:]]*\('"${_target_device}"'[[:alnum:]]\+\)[[:space:]]*:.*LABEL=[[:space:]]*\"\?'"${_boot_bank}"'\"\?.*$|\1|p')"
	fi

	if [ ! -d "${_mountpoint}" ]; then
		mkdir -p "${_output_location}"
	fi

	mount "${_boot_partition}" "${_mountpoint}"
}

lock_boot_device()
{
	_target_device="${1?Missing argument to function}"

	if ! echo "${_target_device}" | grep -E -q '^/dev/(block/)?mmcblk[[:digit:]]+boot[01]$'; then
		return
	fi

	if ! echo "1" > "/sys/block/$(basename "${_target_device}")/force_ro"; then
		e_warn "Failed to enable write protection on boot device '${_target_device}'"
		return
	fi
}

unlock_boot_device()
{
	_target_device="${1?Missing argument to function}"

	if [ -L "${_target_device}" ]; then
		_target_device="$(readlink -f "${_target_device}")"
	fi

	if [ ! -b "${_target_device}" ]; then
		return
	fi

	if ! echo "${_target_device}" | grep -E -q '^/dev/(block/)?mmcblk[[:digit:]]+boot[01]$'; then
		return
	fi

	if ! echo "0" > "/sys/block/$(basename "${_target_device}")/force_ro"; then
		e_warn "Failed to disable write protection on boot device '${_target_device}'"
		return
	fi

	unlocked_boot_device="${unlocked_boot_device:-} ${_target_device}"
}

write_script()
{
	_fw_env_config="${1?Missing argument to function}"
	_input_file="${2?Missing argument to function}"
	_worm="${worm_success:-${3:-false}}"

	if [ ! -f "${_input_file}" ]; then
		e_err "Cannot find '${_input_file}'"
		exit 1
	fi

	if [ ! -f "${_fw_env_config}" ]; then
		e_err "No U-Boot tools configuration file '${_fw_env_config}'"
		exit 1
	fi

	_env_output="$(dirname "$(sed -e 's|#.*$||g' -e '/^$/d' "${_fw_env_config}" | \
	                          cut -f 1 | head -n 1)")"
	if [ "${_env_output}" = "${_env_output#/dev}" ]; then
		e_err "Environments on filesystems not yet supported"
		exit 1
	fi

	unlock_boot_device "${_env_output}"

	if ! ${dry_run:+echo} fw_setenv --config "${_fw_env_config}" \
	                                --lock "/run/" \
	                                --script "${_input_file}"; then
		if [ "${_worm:-}" != "true" ]; then
			e_err "Failed to write, cannot continue"
			exit 1
		fi
	fi
}

empty_env()
{
	_fw_env_config="${1?Missing argument to function}"
	_output_location="${2?Missing argument to function}"
	_byte_offset="${3?Missing argument to function}"
	_worm="${worm_success:-${4:-false}}"

	if [ ! -f "${_fw_env_config}" ]; then
		e_err "Cannot find configuration file '${_fw_env_config}'"
		exit 1
	fi

	# U-Boot's fw_printenv does not fail on a bad CRC, which is an error
	# but we also want to fail if there is not even an environment
	if fw_printenv --config "${_fw_env_config}" \
	               --lock "/run/" 1> "/dev/null" && \
	   ! fw_printenv --config "${_fw_env_config}" \
	                 --lock "/run/" 2>&1 | grep -q "Warning: Bad CRC"; then
		return
	fi

	unlock_boot_device "${_output_location}"

	# shellcheck disable=SC2094  # We only read _fw_env_config here
	while read -r _env_device _env_offset _env_size _; do
		if [ -z "${_env_device}" ] || \
		   [ "${_env_device#/}" = "${_env_device}" ] || \
		   ! is_integer "${_env_offset:=0}" || \
		   ! is_integer "${_env_size:=0}"; then
			continue
		fi

		if [ "$((_env_offset))" -lt 0 ]; then
			if [ -b "${_output_location}" ]; then
				_env_offset="$(($(blockdev --getsize64 "${_output_location}") - -_env_offset))"
			elif [ -f "${_output_location}" ]; then
				_env_offset="$(($(stat -c "%s" "${_output_location}") - -_env_offset))"
			else
				e_err "Negative offsets are only allowed on regular files or block devices"
			fi
		fi

		${dry_run:+ echo} mkenvimage -s "$((_env_size))" -o "${env_temp_location}" <<-EOT
			${DEF_UBOOT_ENV_VER}
		EOT

		for _ in $(seq 1 5); do
			if [ -c "${_env_device}" ]; then
				${dry_run:+ echo} flash_erase "${_env_device}" "$((_env_offset))" 0
			elif [ ! -e "${_env_device}" ] && \
			     [ "${_env_device#/dev}" = "${_env_device}" ]; then
				${dry_run:+ echo} mkdir -p "$(dirname "${_env_device}")"
				${dry_run:+ echo} touch "${_env_device}"
			fi

			if ! ${dry_run:+echo} dd \
			                         bs="$((_env_size))" \
			                         oflag=seek_bytes \
			                         count=1 \
			                         if="${env_temp_location}" \
			                         of="${_output_location}" \
			                         seek="$((_env_offset))"; then
				e_warn "Unable to write env, retrying ..."
				sleep 1
				continue
			fi

			if [ "$(fw_printenv --config "${_fw_env_config}" \
					    --lock "/run/" \
					    "${DEF_UBOOT_ENV_VER%%=*}" | \
				tail -n 1)" = "${DEF_UBOOT_ENV_VER}" ] ; then
				return
			fi

			if [ "${dry_run:-}" = "true" ]; then
				break
			fi

			e_warn "Environment does not contain 'env_version', retrying ..."
			sleep 1
		done
	done < "${_fw_env_config}"

	if [ "${dry_run:-}" != "true" ] && \
	   [ "${_worm:-}" != "true" ]; then
		e_err "Failed to update environment"
		exit 1
	fi
}

write_file()
{
	_input_file="${1?Missing argument to function}"
	_output_location="${2?Missing argument to function}"
	_byte_offset="${3?Missing argument to function}"
	_worm="${worm_success:-${4:-false}}"

	if [ ! -f "${_input_file}" ]; then
		e_err "Cannot find '${_input_file}'"
		exit 1
	fi

	if [ -L "${_output_location}" ]; then
		_output_location="$(readlink -f "${_output_location}")"
	fi
	if [ "${_output_location#/dev/*}" != "${_output_location}" ] && \
	   [ ! -b "${_output_location}" ] && [ ! -c "${_output_location}" ]; then
		e_err "Device '${_output_location}' is not a block or character device"
		exit 1
	fi

	unlock_boot_device "${_output_location}"

	_byte_size="$(stat -c "%s" "${_input_file}")"
	_input_crc="$(md5sum "${_input_file}")"

	echo "Writing '${_input_file}' into '${_output_location}' at offset '${_byte_offset}' "
	if [ -z "${_output_location%/boot/*}" ]; then
		if [ -n "${UBOOT_BOOT_DEVICE:-}" ]; then
			mount_boot "${UBOOT_BOOT_DEVICE}"
			_output_location="${boot_partition_mountpoint}/${_output_location#/boot/}"
		fi
	fi

	if [ -b "${_output_location}" ] && [ "$((_byte_offset))" -lt 0 ]; then
		_byte_offset="$(($(blockdev --getsize64 "${_output_location}") - -_byte_offset))"
	fi

	for _ in $(seq 1 5); do
		_output_crc=""

		if [ -e "${_output_location}" ] && \
		   ! _output_crc="$(tail -c "+$((_byte_offset + 1))" "${_output_location}" | \
		                    head -c "${_byte_size}" | \
		                    md5sum)"; then
			e_warn "Unable to read, retrying ..."
			sleep 1
			continue
		fi

		if [ "${_input_crc%% *}" = "${_output_crc%% *}" ]; then
			break
		fi

		# An empty device can be either all 0x00's or all 0xff's
		if [ -e "${_output_location}" ] && \
		   [ "${_worm:-}" = "true" ] && \
		   [ "$(tail -c "+$((_byte_offset + 1))" "${_output_location}" | \
		        head -c "${_byte_size}" | \
		        tr -d '\0' | tr -d '\377' | \
		        wc -c)" -ne 0 ]; then
			e_warn "Target location contains data, but the WORM flag is set"
			break;
		fi

		if [ -c "${_output_location}" ] && \
		   ! ${dry_run:+ echo} flash_erase "${_output_location}" 0 0; then
			e_warn "Failed to erase flash, retrying ..."
			sleep 1
			continue
		fi

		if [ ! -d "$(dirname "${_output_location}")" ] && \
		   ! ${dry_run:+ echo} mkdir -p "$(dirname "${_output_location}")"; then
			e_warn "Failed to create output directory, retrying ..."
			sleep 1
			continue
		fi

		if ! ${dry_run:+echo} dd \
		                         bs="${_byte_size}" \
		                         oflag=seek_bytes \
		                         conv="fsync,notrunc" \
		                         count=1 \
		                         if="${_input_file}" \
		                         of="${_output_location}" \
		                         seek="$((_byte_offset))"; then
			e_warn "Unable to write, retrying ..."
			sleep 1
			continue
		fi

		if [ "${dry_run:-}" = "true" ]; then
			break
		fi
	done

	if mountpoint -q "${boot_partition_mountpoint}"; then
		umount "${boot_partition_mountpoint:-}"
	fi

	if [ "${_input_crc%% *}" != "${_output_crc%% *}" ] && \
	   [ "${dry_run:-}" != "true" ]; then
		if [ "${_worm:-}" != "true" ]; then
			e_err "Failed to write, cannot continue"
			exit 1
		fi
		e_warn "CRC mismatch on read-only target, unable to update '${_input_file}'"
		return
	fi
}

enable_mmcboot_partition()
{
	_boot_part="${1?Missing argument to function}"
	_mmc_device="${2?Missing argument to function}"
	_mmc_bootpart=0

	if [ "${_boot_part}" -eq 0 ]; then
		_mmc_bootpart=1
	elif [ "${_boot_part}" -eq 1 ]; then
		_mmc_bootpart=2
	else
		e_err "Invalid boot partition selected"
		exit 1
	fi

	if ! mmc bootpart enable "${_mmc_bootpart}" 0 "${_mmc_device}"; then
		e_err "Failed to enable mmc boot partition ${_boot_part}"
		exit 1
	fi
}

switch_boot_partition()
{
	_bin_location="${1:-}"
	_bin_copy_location="${2:-}"

	if [ -z "${_bin_location:-}" ] || \
	   [ -z "${_bin_copy_location:-}" ]; then
		echo "Not switching eMMC partitions without copy"
		return
	fi

	if ! echo "${_bin_location}" | grep -E -q '^/dev/mmcblk[[:digit:]]+boot[01]$' ||
	   ! echo "${_bin_copy_location}" | grep -E -q '^/dev/mmcblk[[:digit:]]+boot[01]$'; then
		echo "Not an eMMC boot hardware partition"
		return
	fi

	_mmc_device="${_bin_copy_location%boot[01]}"
	_boot_part="${_bin_copy_location#/dev/mmcblk[[:digit:]]boot}"
	enable_mmcboot_partition "${_boot_part}" "${_mmc_device}"

	echo "Switched to mmc boot partition ${_boot_part}"
}

update_uboot()
{
	echo "Writing components ..."

	if [ -n "${UBOOT_ENV:-}" ]; then
		echo "    U-Boot environment:"
		write_file "${UBOOT_ENV}" \
		           "${UBOOT_ENV_LOCATION}" \
		           "${UBOOT_ENV_OFFSET:-0}" \
		           "${UBOOT_ENV_WORM:-}"
		_uboot_updated="true"
	elif [ -n "${UBOOT_ENV_LOCATION:-}" ]; then
		echo "    U-Boot empty environment:"
		empty_env "${UBOOT_ENV_CONFIG:-/etc/fw_env.config}" \
		          "${UBOOT_ENV_LOCATION}" \
		          "${UBOOT_ENV_OFFSET:-0}" \
		          "${UBOOT_ENV_WORM:-}"
		_uboot_updated="true"
	fi

	if [ -n "${UBOOT_ENV_COPY:-}" ]; then
		echo "    U-Boot environment (secondary copy):"
		write_file "${UBOOT_ENV_COPY}" \
		           "${UBOOT_ENV_COPY_LOCATION}" \
		           "${UBOOT_ENV_COPY_OFFSET:-0}" \
		           "${UBOOT_ENV_COPY_WORM:-}"
		_uboot_updated="true"
	elif [ -n "${UBOOT_ENV_COPY_LOCATION:-}" ]; then
		echo "    U-Boot empty environment (secondary copy):"
		empty_env "${UBOOT_ENV_COPY_CONFIG:-/etc/fw_env_copy.config}" \
		          "${UBOOT_ENV_COPY_LOCATION}" \
		          "${UBOOT_ENV_COPY_OFFSET:-0}" \
		          "${UBOOT_ENV_COPY_WORM:-}"
		_uboot_updated="true"
	fi

	if [ -n "${UBOOT_ENV_SCRIPT:-}" ]; then
		echo "    U-Boot environment script:"
		write_script "${UBOOT_ENV_CONFIG:-/etc/fw_env.config}" \
		             "${UBOOT_ENV_SCRIPT}" \
		             "${UBOOT_ENV_WORM:-}"
		_uboot_updated="true"
	fi

	if [ -n "${UBOOT_ENV_COPY_SCRIPT:-}" ]; then
		echo "    U-Boot environment script (secondary copy):"
		write_script "${UBOOT_ENV_COPY_CONFIG:-/etc/fw_env_copy.config}" \
		             "${UBOOT_ENV_COPY_SCRIPT}" \
		             "${UBOOT_ENV_COPY_WORM:-}"
		_uboot_updated="true"
	fi

	if [ -n "${UBOOT_BIN:-}" ]; then
		echo "    U-Boot proper:"
		switch_boot_partition "${UBOOT_BIN_LOCATION:-}" "${UBOOT_BIN_COPY_LOCATION:-}"
		write_file "${UBOOT_BIN}" \
		           "${UBOOT_BIN_LOCATION}" \
		           "${UBOOT_BIN_OFFSET:-0}" \
		           "${UBOOT_BIN_WORM:-}"
		switch_boot_partition "${UBOOT_BIN_COPY_LOCATION:-}" "${UBOOT_BIN_LOCATION:-}"
		_uboot_updated="true"
	fi

	if [ -n "${UBOOT_BIN_COPY:-}" ]; then
		echo "    U-Boot proper (secondary copy):"
		write_file "${UBOOT_BIN_COPY}" \
		           "${UBOOT_BIN_COPY_LOCATION}" \
		           "${UBOOT_BIN_COPY_OFFSET:-0}" \
		           "${UBOOT_BIN_COPY_WORM:-}"
		_uboot_updated="true"
	fi

	if [ -n "${UBOOT_TPL:-}" ]; then
		echo "    U-Boot TPL:"
		switch_boot_partition "${UBOOT_TPL_LOCATION:-}" "${UBOOT_TPL_COPY_LOCATION:-}"
		write_file "${UBOOT_TPL}" \
		           "${UBOOT_TPL_LOCATION}" \
		           "${UBOOT_TPL_OFFSET:-0}" \
		           "${UBOOT_TPL_WORM:-}"
		switch_boot_partition "${UBOOT_TPL_COPY_LOCATION:-}" "${UBOOT_TPL_LOCATION:-}"
		_uboot_updated="true"
	fi

	if [ -n "${UBOOT_TPL_COPY:-}" ]; then
		echo "    U-Boot TPL (secondary copy):"
		write_file "${UBOOT_TPL_COPY}" \
		           "${UBOOT_TPL_COPY_LOCATION}" \
		           "${UBOOT_TPL_COPY_OFFSET:-0}" \
		           "${UBOOT_TPL_COPY_WORM:-}"
		_uboot_updated="true"
	fi

	if [ -n "${UBOOT_SPL:-}" ]; then
		echo "    U-Boot SPL:"
		switch_boot_partition "${UBOOT_SPL_LOCATION:-}" "${UBOOT_SPL_COPY_LOCATION:-}"
		write_file "${UBOOT_SPL}" \
		           "${UBOOT_SPL_LOCATION}" \
		           "${UBOOT_SPL_OFFSET:-0}" \
		           "${UBOOT_SPL_WORM:-}"
		switch_boot_partition "${UBOOT_SPL_COPY_LOCATION:-}" "${UBOOT_SPL_LOCATION:-}"
		_uboot_updated="true"
	fi

	if [ -n "${UBOOT_SPL_COPY:-}" ]; then
		echo "    U-Boot SPL (secondary copy):"
		write_file "${UBOOT_SPL_COPY}" \
		           "${UBOOT_SPL_COPY_LOCATION}" \
		           "${UBOOT_SPL_COPY_OFFSET:-0}" \
		           "${UBOOT_SPL_COPY_WORM:-}"
		_uboot_updated="true"
	fi

	if [ "${_uboot_updated:-}" != "true" ]; then
		e_err "Nothing to write, is 'board.config' setup properly?"
		exit 1
	fi

	echo "Done writing"
}

get_config()
{
	if [ ! -f "/sys/firmware/devicetree/base/compatible" ]; then
		e_err "Unable to obtain 'compatible' node from the devicetree, is sysfs mounted at '/sys'?"
		exit 1
	fi

	for _compatible in $(tr '\000' '\n' < "/sys/firmware/devicetree/base/compatible" | tac); do
		if [ -n "${_compatible:-}" ] && \
		   [ -f "${uboot_path}/${_compatible}/board.config" ]; then
			# shellcheck source=/dev/null
			. "${uboot_path}/${_compatible}/board.config"
			_found_board="true"
		fi
	done

	if [ "${_found_board:-}" != "true" ]; then
		e_err "Unable to find a compatible for this board."
		exit 1
	fi
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":fhu:y" options; do
		case "${options}" in
		f)
			worm_success="false"
			;;
		h)
			usage
			exit 0
			;;
		u)
			uboot_path="$(readlink -f "${OPTARG}")"
			;;
		y)
			dry_run="true"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	uboot_path="${uboot_path:-${UBOOT_PATH:-${DEF_UBOOT_PATH}}}"
	worm_success="${worm_success:-${WORM_FAILS:+false}}"

	check_requirements
	init
	get_config
	update_uboot
	cleanup
}

main "${@}"

exit 0
