#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2020 Thibault Ferrante <thibault.ferrante@pm.me>

set -eu

COMPATIBLES_PATH="/sys/firmware/devicetree/base/compatible"
DEF_UPDATER_PATH="/usr/share/updater/"
DEF_UBOOT_PATH="/usr/share/u-boot/"
REQUIRED_COMMANDS="
	[
	command
	dd
	echo
	exit
	fw_printenv
	fw_setenv
	getopts
	grep
	mktemp
	printf
	return
	sed
	set
	sfdisk
	tac
	test
	tr
	trap
"
UINT32_MAX="0xffffffff"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	>&2 echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo
	echo "Get/Update boot information"
	echo "    -b  Get target bank"
	echo "    -d  Get target device"
	echo "    -h  Print this help text and exit"
	echo "    -p  Path to updater configuration files (default: '${DEF_UPDATER_PATH}') [UPDATER_PATH]"
	echo "    -s  Swap boot banks, boot from the alternative bank next reboot"
	echo "    -u  U-Boot board configuration files path (default: '${DEF_UBOOT_PATH}') [UBOOT_PATH]"
	echo "    -w  Get write count"
	echo "All options listed between [brackets] can also be passed in environment variables."
}

is_integer()
{
	__val="${1?Missing argument to function}"

	test "$((__val))" -eq "$((__val))" 2> "/dev/null"
}

cleanup()
{
	if [ -f "${script_file:-}" ]; then
		unlink "${script_file}"
	fi

	if [ -n "${unlocked_boot_device:-}" ]; then
		for _boot_device in ${unlocked_boot_device}; do
			lock_boot_device "${_boot_device}"
		done
	fi

	trap EXIT
}

init()
{
	trap cleanup EXIT

	script_file="$(mktemp -p "${TMPDIR:-/tmp}" "script_file.XXXXXX")"

	get_config "${uboot_path}" "board.config"
	get_config "${updater_path}" "boot.config"

	if ! get_write_count || [ "${write_count:=0}" -le 0 ]; then
		e_warn "Failed to get write counter, using defaults"
	fi
}

get_config()
{
	_config_path=${1?Missing argument to function}
	_config_file_name=${2?Missing argument to function}

	if [ ! -f "${COMPATIBLES_PATH}" ]; then
		e_err "Unable to obtain 'compatible' node from the devicetree, is sysfs mounted at '/sys'?"
		exit 1
	fi

	for _compatible in $(tr '\0' '\n' < "${COMPATIBLES_PATH}" | tac); do
		if [ -n "${_compatible:-}" ] && \
		   [ -f "${_config_path}/${_compatible}/${_config_file_name}" ]; then
			# shellcheck source=/dev/null
			. "${_config_path}/${_compatible}/${_config_file_name}"
			_found_config="true"
		fi
	done

	if [ "${_found_config:-}" != "true" ]; then
		e_err "Unable to find a compatible '${_config_file_name}'."
		exit 1
	fi
}

valid_env()
{
	_target_env="${1?Missing argument to function}"

	if ! _write_count="$(fw_printenv --config "${_target_env}" --lock "/run/" --noheader "${ENVIRONMENT_WRITE_COUNT_NAME}")" || \
	   fw_printenv --config "${_target_env}" --lock "/run/" 2>&1 | \
	   grep -q "Bad CRC"; then
		return 1
	fi

	return 0
}

check_env_consistency()
{
	unset _env_content
	unset _env_content_copy

	if [ -n "${UBOOT_ENV_CONFIG:-}" ] && \
	   [ -n "${UBOOT_ENV_COPY_CONFIG:-}" ]; then
		_env_content="$(fw_printenv --config "${UBOOT_ENV_CONFIG}" --lock "/run/")"
		_env_content_copy="$(fw_printenv --config "${UBOOT_ENV_COPY_CONFIG}" --lock "/run/")"
		if [ "${_env_content}" != "${_env_content_copy}" ]; then
			return 1
		fi
	fi

	return 0
}

check_bootargs_consistency()
{
	_root_bootarg="$(tr " " "\n" < "/proc/cmdline"  | grep root)"
	_root_value="${_root_bootarg##root=}"
	_target_device="${_root_value%p*}"
	if [ -z "${_target_device}" ]; then
		e_warn "No 'root' parameter found"
		return 0
	fi

	_root_label="$(sfdisk --dump --quiet "${_target_device}" | \
	               grep "${_root_value}" | \
	               sed -n 's|^[[:space:]]*\/dev\/[[:alnum:]]\+[[:space:]]*:.*name=[[:space:]]*\"\?\([[:alnum:]_]*\)\"\?.*$|\1|p')"
	_root_bank="${_root_label##*_}"
	if [ -z "${_root_bank}" ]; then
		e_warn "The label of the 'root' argument doesn't match a partition"
		return 0
	fi

	_current_active_stored_bank="$(get_current_bank)"
	if [ "${_current_active_stored_bank}" != "${_root_bank}" ]; then
		return 1
	fi
}

get_absolute_offset()
{
	_device="${1?Missing argument to function}"
	_offset="${2?Missing argument to function}"

	if [ "$((_offset))" -lt 0 ]; then
		if [ -b "${_device}" ]; then
			_offset="$(($(blockdev --getsize64 "${_device}") - -_offset))"
		elif [ -f "${_device}" ]; then
			_offset="$(($(stat -c "%s" "${_device}") - -_offset))"
		else
			e_err "Negative offsets are only allowed on regular files or block devices"
			return 1
		fi
	fi

	printf "%d" "${_offset}"
}

copy_env()
{
	_config_env_src="${1?Missing argument to function}"
	_config_env_dest="${2?Missing argument to function}"

	while read -r _device_src _offset_src _size_src _; do
		if [ -z "${_device_src}" ] || \
		   [ "${_device_src#/}" = "${_device_src}" ] || \
		   ! is_integer "${_offset_src:=0}" || \
		   ! is_integer "${_size_src:=0}"; then
			continue
		fi

		if ! _offset_src="$(get_absolute_offset "${_device_src}" "${_offset_src}")"; then
			continue
		fi

		_found_valid_origin_env="true"

		break;
	done < "${_config_env_src}"

	if [ -z "${_found_valid_origin_env:-}" ]; then
		e_err "Unable to repair boot env, source environment appears to be broken"
		exit 1
	fi

	unlock_boot_device "${_config_env_dest}"
	while read -r _device_dest _offset_dest _size_dest _; do
		if [ -z "${_device_dest}" ] || \
		   [ "${_device_dest#/}" = "${_device_dest}" ] || \
		   ! is_integer "${_offset_dest:=0}" || \
		   ! is_integer "${_size_dest:=0}"; then
			continue
		fi

		if ! _offset_dest="$(get_absolute_offset "${_device_dest}" "${_offset_dest}")"; then
			continue
		fi

		for _ in $(seq 1 5); do
			if [ -c "${_device_dest}" ]; then
				flash_erase "${_device_dest}" "$((_offset_dest))" 0
			elif [ ! -e "${_device_dest}" ] && \
			     [ "${_device_dest#/dev}" = "${_device_dest}" ]; then
				mkdir -p "$(dirname "${_device_dest}")"
				touch "${_device_dest}"
			fi

			if ! dd \
			         bs="$((_size_src))" \
			         oflag=seek_bytes \
			         count=1 \
			         if="${_device_src}" \
			         of="${_device_dest}" \
			         seek="$((_offset_dest))"; then
				e_warn "Unable to write env, retrying ..."
				sleep 1
				continue
			fi

			break
		done
	done < "${_config_env_dest}"

	if ! valid_env "${_config_env_dest}"; then
		return 1
	fi
}

repair_env()
{
	if [ "${valid_configuration_file}" = "${UBOOT_ENV_COPY_CONFIG}" ]; then
		invalid_configuration_file="${UBOOT_ENV_CONFIG}"
	else
		invalid_configuration_file="${UBOOT_ENV_COPY_CONFIG}"
	fi

	if ! copy_env "${valid_configuration_file}" "${invalid_configuration_file}"; then
		e_warn "Failed to repair environment"
	fi

	if ! check_env_consistency; then
		e_warn "Unable to have consistent environment"
		if valid_env "${invalid_configuration_file}" && \
		   valid_env "${valid_configuration_file}"; then
			return 1
		fi
	fi
}

get_write_count()
{
	for _config_env in ${UBOOT_ENV_CONFIG:-} ${UBOOT_ENV_COPY_CONFIG:-}; do
		if [ ! -f "${_config_env}" ] || \
		   [ ! -r "${_config_env}" ]; then
			e_warn "Unable to find '${_config_env}', skipping ..."
			continue
		fi

		if ! valid_env "${_config_env}"; then
			e_warn "Failure with boot environment '${_config_env}'"
			continue
		fi

		if ! is_integer "${_write_count:-false}" ; then
			_write_count=0
		fi

		_write_count="$((_write_count))"

		if [ "${_write_count}" -gt "${write_count:-0}" ]; then
			write_count="${_write_count}"
			valid_configuration_file="${_config_env}"
		fi
	done

	if [ "${write_count:=0}" -le 0 ]; then
		return 1
	fi
}

get_env_var()
{
	_variable_name="${1?Missing argument to function}"
	_default_value="${2?Missing argument to function}"

	if [ -z "${valid_configuration_file:-}" ] || \
	   ! _var="$(fw_printenv --config "${valid_configuration_file:-}" \
                                 --lock "/run/" \
                                 --noheader \
                                 "${_variable_name}")" ;then
		_var="${_default_value}"
	fi

	printf "%s" "${_var}"
}

get_target_bank()
{
	if [ -z "${DEFAULT_BANK:-}" ] || \
	   [ -z "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME:-}" ] || \
	   [ -z "${UBOOT_ENV_CONFIG:-}" ]; then
		e_err "Missing bank configuration, is 'boot.config' setup properly?"
		exit 1
	fi

	get_env_var "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME}" "${DEFAULT_BANK}"
}

get_current_bank()
{
	if [ -z "${DEFAULT_BANK_ALT:-}" ] || \
	   [ -z "${ENVIRONMENT_BANK_VARIABLE_NAME:-}" ] || \
	   [ -z "${UBOOT_ENV_CONFIG:-}" ]; then
		e_err "Missing bank configuration, is 'boot.config' setup properly?"
		exit 1
	fi

	get_env_var "${ENVIRONMENT_BANK_VARIABLE_NAME}" "${DEFAULT_BANK_ALT}"
}

is_comment()
{
    test -z "${1%%#*}"
}

lock_boot_device()
{
	_target_device="${1?Missing argument to function}"

	if ! echo "1" > "/sys/block/$(basename "${_target_device}")/force_ro"; then
		e_warn "Failed to enable write protection on boot device '${_target_device}'"
		return
	fi
}

unlock_boot_device()
{
	_configuration_file="${1?Missing argument to function}"
	while read -r _path _; do
		if [ -z "${_path:-}" ] || is_comment "${_path}"; then
			continue
		fi

		if [ -L "${_path}" ]; then
			_path="$(readlink -f "${_path}")"
		fi

		if [ ! -b "${_path}" ]; then
			continue
		fi

		if ! echo "${_path}" | grep -E -q '^/dev/(block/)?mmcblk[[:digit:]]+boot[01]$'; then
			continue
		fi

		if ! echo "0" > "/sys/block/$(basename "${_path}")/force_ro"; then
			e_warn "Failed to disable write protection on boot device '${_path}'"
			continue
		fi

		unlocked_boot_device="${unlocked_boot_device:-} ${_path}"
	done < "${_configuration_file}"
}

update_single_env()
{
	_config_env="${1?Missing argument to function}"
	if [ ! -f "${_config_env}" ] || \
	   [ ! -r "${_config_env}" ]; then
		e_warn "Unable to find '${_config_env}', skipping ..."
		return 1
	fi

	for _ in $(seq 1 5); do
		unlock_boot_device "${_config_env}"
		if ! fw_setenv --config "${_config_env}" \
		               --lock "/run/" \
		               --script "${script_file}"; then
			e_warn "Failed to write environment into '${_config_env}', retrying ..."
			sleep 1
			continue
		fi

		while read -r _key _value; do
			if ! _verify="$(fw_printenv --config "${_config_env}" \
			                            --lock "/run" \
			                            --noheader \
			                            "${_key}")" || \
			   [ "${_verify:-}" != "${_value}" ]; then
				e_warn "Failed to verify environment value '${_key}': '${_verify}' != '${_value}', retrying ..."
				sleep 1
				continue
			fi
		done < "${script_file}"

		echo "Environment '${_config_env}' updated successfully"

		return 0
	done

	return 1
}

swap_banks()
{
	if [ -z "${ENVIRONMENT_BANK_VARIABLE_NAME:-}" ] || \
	   [ -z "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME:-}" ] || \
	   [ -z "${UBOOT_ENV_CONFIG:-}" ]; then
		e_err "Missing bank configuration, is 'boot.config' setup properly?"
		exit 1
	fi

	_boot_env_updated=0
	_target_bank="$(get_target_bank)"
	_target_bank_alt="$(get_current_bank)"

	if [ "${write_count}" -ge "$((UINT32_MAX))" ]; then
		_incr_write_count=1
	else
		_incr_write_count="$((write_count + 1))"
	fi

	{
		printf "%s %s\n" "${ENVIRONMENT_BANK_VARIABLE_NAME}" "${_target_bank}"
		printf "%s %s\n" "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME}" "${_target_bank_alt}"
		printf "%s 0x%x\n" "${ENVIRONMENT_WRITE_COUNT_NAME}" "${_incr_write_count}"
	} > "${script_file}"

	if ! update_single_env "${UBOOT_ENV_CONFIG}" ; then
		if ! valid_env "${UBOOT_ENV_CONFIG}"; then
			e_err "Unable to write updated environment '${UBOOT_ENV_CONFIG}' to repair outdated values"
			e_err "Critical error, swapping banks aborted"
			exit 1
		fi
		e_warn "Unable to write U-Boot environment '${UBOOT_ENV_CONFIG}', continuing anyway"
	else
		_boot_env_updated="$((_boot_env_updated + 1))"
	fi

	if [ -n "${UBOOT_ENV_COPY_CONFIG:-}" ]; then
		if ! update_single_env "${UBOOT_ENV_COPY_CONFIG}" ; then
			if ! valid_env "${UBOOT_ENV_COPY_CONFIG}"; then
				e_warn "Unable to write U-Boot environment '${UBOOT_ENV_COPY_CONFIG}', and it holds outdated values"
			else
				e_warn "Unable to write U-Boot environment '${UBOOT_ENV_COPY_CONFIG}', continuing anyway"
			fi
		else
			_boot_env_updated="$((_boot_env_updated + 1))"
		fi
	fi

	if [ "${_boot_env_updated}" -le 0 ]; then
		e_err "Unable to write on any boot environment"
		exit 1
	fi

	write_count="${_incr_write_count}"
}

get_target_device()
{
	if [ -z "${UPDATE_TARGET_DEVICE:-}" ]; then
		e_err "No UPDATE_TARGET_DEVICE available, is 'boot.config' setup properly?"
		exit 1
	fi

	printf "%s" "${UPDATE_TARGET_DEVICE}"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":bdhp:su:w" options; do
		case "${options}" in
		b)
			_action="get_target_bank"
			;;
		d)
			_action="get_target_device"
			;;
		h)
			usage
			exit 0
			;;
		p)
			updater_path="${OPTARG}"
			;;

		s)
			_action="swap_banks"
			;;
		u)
			uboot_path="${OPTARG}"
			;;
		w)
			_action="get_write_count"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done

	uboot_path="${uboot_path:-${UBOOT_PATH:-${DEF_UBOOT_PATH}}}"
	updater_path="${updater_path:-${UPDATER_PATH:-${DEF_UPDATER_PATH}}}"

	check_requirements
	init

	if [ -z "${valid_configuration_file:-}" ]; then
		e_warn "Unable to determine valid environment"
	else
		if ! check_bootargs_consistency; then
			e_warn "Environment not consistent with bootargs, try to swap_banks to fix it."
			if ! swap_banks > "/dev/null" 2>&1 ; then
				e_err "Unable to swap banks, the system is inconsistent and beyond repair."
				exit 1
			fi
		fi
		if ! check_env_consistency; then
			e_warn "Inconsistent environment detected, attempting repair"
			if ! repair_env; then
				e_err "Invalid environment can't be repaired and stores invalid values"
				exit 1
			fi
		fi
	fi

	case "${_action:-}" in
	get_target_device)
		get_target_device
		;;
	get_target_bank)
		get_target_bank
		;;
	get_write_count)
		printf "Write counter is currently at '%d'\n" "${write_count}"
		;;
	swap_banks)
		swap_banks
		;;
	*)
		e_err "Unknown option"
		usage
		exit 1
		;;
	esac
}

main "${@}"

exit 0
