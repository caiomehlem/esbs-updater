# Software updater
Installing an embedded platform is always a complex problem. This project
intends to make this easier to install software. While at first glance, this
package appears to install an update, an installation is basically the same.
The target platform is simply empty, and thus an update is performed on an
empty system.

## Update verification
Before being able to perform an update, it is wise to perform a sanity check on
the software update file. A software update file consists of five parts.
* The update itself packaged as squashfs image, the payload
* Chain of certificate including the leaf certificate
* 4 bytes in little-endian indicating the size of the chain of certificate
* An OpenSSL signature
* 4 bytes in little-endian indicating the size of the signature

All these parts are concatenated together to form a software-update file.

To prove the validity of the file, all five of these parts need to be in order.
If the size is wrong, an invalid signature would be assessed. An invalid
signature means the signature validation fails of the file. The same goes for an
invalid signature, or an corrupted payload.

It thus is wise to always check the validity of the update, as not only
authenticity is checked, but also against file corruption.

### Chain of trust
To verify an update file, the file *validate_chain.sh* needs to be available on
the device performing the actual update. Furthermore the root certificate
should be pre-installed on the device. In this case, run
`validate_chain.sh -r <root_certificate> <update.swu>`.
On success the script will return 0. An error is returned otherwise.

Be aware that this script doesn't use the trust-store(/etc/ssl/certs) as input and relies
only on the root certificate used as an argument.

To extract the public_key from the leaf certificate used to sign the software update file,
use `-p` parameter where the public key should be extracted:
`validate_chain.sh -r <root_certificate> -p <public_key> <update.swu>`.
This public key will be used by *verify_update.sh* in the next step([verification](#verification)).

### Verification
To verify an update file, the file *verify_update.sh* needs to be available on
the device performing the actual update. Further more, the public key of the
private key used to create the signature needs to be on the device as well.
All that is needed then, is to run `verify_update.sh -p <public_key> <update.swu>`.
On success the script will return 0. An error is returned otherwise.

## Start of update
To start an update, a software update file (.swu) needs to be passed to the
*start_update.sh* script, it can be provided with a target device to perform the
software update on/to. If not provided, `boot.config` file will be used as input,
residing in `/usr/share/updater/<compatible>`. The compatible name matches
to one of the boards compatibles. Compatibles are defined in the devicetree
and read using `/sys/firmware/devicetree/base/compatible`.
The script itself is kept as simple as possible, as the script will be available
on the device already. As such, having any logic in the update script also means
that it may not be available on the target, a so called 'chicken and egg' problem.

## Archive updates
The *start_update.sh* script will prepare the update environment and call the
*system_update.sh* script from within the archive. It is this script that
contains all the updating logic. One of the main responsibilities of the
`system_update.sh` script is to execute all update step-scripts found in
`/usr/libexec/system_update.d/`.

## Disk preparation
### Partitioning
The disk preparation script expects to find a file matching the target device,
ending with the .sfdisk extension. The partition format is exactly the output
of `sfdisk --dump`, hence the name. Only if both the target device or
partitioning file are present partitioning can start.

As partitioning currently depends on *sfdisk*, only *sfdisk* supported targets
can currently be used.

### Formatting
To determine what filesystem is to be used, the *name* field in the partition
file is used. While untested, **MBR** type of partition tables should work, but
they do need the name field added to each record.

Only **ext4** and **f2fs** are supported as filesystems. The disk preperation
script prepares or formats disks based on the *name*. Currently only the *boot*
partition is formatted as **ext4** as *U-Boot* lacks support for **f2fs**. The
following partition labels will be formatted as **f2fs*

- cache
- data
- log
- tmp

For example partition files see *test/fixtures/* for partitioning files.

### Provisioning of the rootfs
The rootfs is to be a **squashfs** based filesystem and it is placed into
partition with the *rom_* prefix. With `boot.config` file residing in
`/usr/share/updater/<compatible>`, u-boot environment is used to get current
bank used. If there is no environment, bank "a" is used. Whatever **squashfs**
filesystem is used (e.g. options and compressions) kernel support in the running
kernel is required. So when wanting to change for example the compression type,
extra care needs to be taken that the current kernel, does already support the
newly desired compression type.

### Provisioning of u-boot
As each board can have its files and location differently, it is expected that
each board supplies this basic information in a `board.config` file residing in
`/usr/share/u-boot/<compatible>`. The compatible name matches to one of the
boards compatibles. Compatibles are defined in the devicetree and read using
`/sys/firmware/devicetree/base/compatible`. A board can have multiple
compatibles, from more generic to more specific and variables are inherited in
the same way.

### Board configuration
The file `board.config` is required to share important variables regarding
the used board, such as locations for the various U-Boot components. The file
in itself is sourced internally as a shell script, where the following
variables are expected as can be seen in this full example.

```
UBOOT_ENV_CONFIG="/etc/fw_env.config"
UBOOT_ENV_COPY_CONFIG="/etc/fw_env_copy.config"

UBOOT_BOOT_DEVICE="/dev/mmcblk0p1"

UBOOT_SPL="/usr/share/u-boot/.../SPL"
UBOOT_SPL_LOCATION="/dev/mtdblock0"
UBOOT_SPL_WORM="true"

UBOOT_SPL_COPY="/usr/share/u-boot/.../SPL"
UBOOT_SPL_COPY_LOCATION="/dev/mtdblock1"
UBOOT_SPL_COPY_WORM="true"

UBOOT_TPL="/usr/share/u-boot/.../TPL"
UBOOT_TPL_LOCATION="/boot/TPL"
UBOOT_TPL_WORM="true"

UBOOT_BIN="/usr/share/u-boot/.../u-boot.img"
UBOOT_BIN_LOCATION="/dev/mmcblk0boot0"
UBOOT_BIN_OFFSET=393216

UBOOT_BIN_COPY="/usr/share/u-boot/.../u-boot.img"
UBOOT_BIN_COPY_LOCATION="/dev/mmcblk0boot1"
UBOOT_BIN_COPY_OFFSET=393216

UBOOT_ENV_LOCATION="/dev/mmcblk0boot0"
UBOOT_ENV_OFFSET="-0x20000"

UBOOT_ENV_COPY_LOCATION="/dev/mmcblk0boot1"
UBOOT_ENV_COPY_OFFSET="-0x20000"

UBOOT_ENV_SCRIPT="/usr/share/u-boot/.../u-boot.scr"
```

Note, that in the example above, mtdblock is used rather then the raw mtd
device. Both are supported and allowed, where raw mtd devices are being
`flash_erased` before writing.

The variable `UBOOT_BOOT_DEVICE` relates to directory based locations, the
later being determined that the location is `/boot` and not a *device*.

Each component, consists of a source file, a `_LOCATION` where to write to,
an `_OFFSET`, within this location and a `_WORM` indicator for *write once
read many times. This might be a little a misnomer as this can also be used
to indicate that write failures are allowed. Use with care. Additionally
with the flag `-f` to force failure on these cases.

Omitted source files (currently only valid for the environment) means that a
potentially corrupted/bad/invalid environment will be replaced by an empty
environment. Especially *new* targets this is useful, where a valid
environment is desired, but should not contain data.

Omitted `_LOCATION` are not allowed, with the exception of scripts, as they
take their input from `UBOOT_ENV_CONFIG`.

Omitted `_OFFSET` defaults to an offset of **0**.

Omitted `_WORM` indicates that write-failures are critical errors.

Each component is in itself optional. E.g. the SPL may not be needed or
desired, and only a `UBOOT_ENV_SCRIPT` may be needed. However the
U-Boot update script itself does require at least one component to not fail.

## Tests
Several tests are included in this repository in the *test* sub-directory. These
are based on shunit2 and can be run either individually, or using docker via the
`run_tests.sh` script.

### Fixtures
The tests require several fixtures to be available, such as a pre-generated
squashfs file and PKI infrastructure configuration files.

Updating of the test files should not be needed to be done very often, as the
content is irrelevant for the update verification test. To generate or modify
the squashfs archive, either `unsquashfs` the existing archive, modify and
create a new squashfs using `mksquashfs <source_dir> <dest_file> -comp xz`.

A simple helper script can be found in `test/fixtures/mksquashfs_update.sh` to
create the archive whilst also adding a few needed files.

## Update file format
An update file consists of several components. There is of course the payload,
a squashfs file system here. Following the payload, there is always 4 zero bytes
to indicate no additional components exist. Appended to the payload + marker
components can be added. Currently, it is followed by the certificate chain,
4 bytes chain size, a signature and a 4 byte signature size.

```
+----------------+---------+
| Payload        | n-bytes |
| Zero marker    | 4 bytes |
+----------------+---------+
| Cert chain     | n-bytes |
| Chain size     | 4 bytes |
+----------------+---------+
| Signature      | m-bytes |
| Signature size | 4 bytes |
+----------------+---------+
```

This structure is chosen, so that it is possible to read data from the end of
the file, and parse it backwards, allowing for cheap manipulation. In the case
of a squashfs payload, nothing is needed, it can simply be mounted. Other
payloads may decide to truncate the file at the zero byte marker.
Further more inserting data allows for backwards compatibility. A newer firmware
update would try to decode the number of bytes before the signature and get
the zero marker instead, indicating that whatever payload it did expect is not
there.
